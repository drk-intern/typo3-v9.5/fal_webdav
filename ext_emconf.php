<?php

########################################################################
# Extension Manager/Repository config file for ext "fal_webdav".
#
# Auto generated 22-12-2011 20:55
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF['fal_webdav'] = array(
    'title' => 'WebDAV driver for FAL',
    'description' => 'Provides a WebDAV driver for the TYPO3 File Abstraction Layer.',
    'category' => 'be',
    'author' => 'Andreas Wolf',
    'author_email' => 'andreas.wolf@typo3.org',
    'state' => 'beta',
    'author_company' => '',
    'version' => '12.10.0',
    'constraints' => array(
        'depends' => array(
            'typo3' => '10.4.0-10.4.99',
        ),
        'conflicts' => array(
        ),
        'suggests' => array(
        ),
    ),
    '_md5_values_when_last_written' => 'a:5:{s:9:"ChangeLog";s:4:"6f31";s:10:"README.txt";s:4:"ee2d";s:12:"ext_icon.gif";s:4:"1bdc";s:19:"doc/wizard_form.dat";s:4:"c0c7";s:20:"doc/wizard_form.html";s:4:"4670";}',
);
