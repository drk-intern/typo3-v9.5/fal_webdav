<?php
namespace TYPO3\FalWebdav\Backend;

use TYPO3\CMS\Reports\StatusProviderInterface;
use TYPO3\FalWebdav\Utility\EncryptionUtility;
use TYPO3\CMS\Reports\Status;
use TYPO3\CMS\Core\Utility\GeneralUtility;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Andreas Wolf
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices to the license
 *  from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
class StatusReport implements StatusProviderInterface
{
    /**
     * Returns the status of an extension or (sub)system
     *
     * @return array An array of tx_reports_reports_status_Status objects
     */
    public function getStatus()
    {
        return array(
            'mcryptAvailability' => $this->getMcryptAvailability()
        );
    }

    protected function getMcryptAvailability()
    {
        $mcryptAvailable = EncryptionUtility::isMcryptAvailable();
        $severity = $mcryptAvailable === true ? Status::OK : Status::ERROR;
        $status = ($mcryptAvailable ? '' : 'Not ') . 'Available';

        return GeneralUtility::makeInstance(Status::class, 'PHP extension mcrypt', $status, '', $severity);
    }
}
