<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

/** @var \TYPO3\CMS\Core\Resource\Driver\DriverRegistry $registry */
$registry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
    \TYPO3\CMS\Core\Resource\Driver\DriverRegistry::class
);
$registry->registerDriverClass(
    \TYPO3\FalWebdav\Driver\WebDavDriver::class,
    'WebDav',
    'WebDAV',
    'FILE:EXT:fal_webdav/Configuration/FlexForm/WebDavDriverFlexForm.xml'
);

#$TYPO3_CONF_VARS['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass']['fal_webdav'] = \TYPO3\FalWebdav\Backend\TceMainHook::class;

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['clearCachePostProc']['fal_webdav']
    = \TYPO3\FalWebdav\Hooks\T3libTcemainHook::class . '->clearCachePostProc';

    // Cache configuration, see http://wiki.typo3.org/Caching_Framework
if (!isset($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['tx_falwebdav_directorylisting'])) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['tx_falwebdav_directorylisting'] = array(
        'frontend' => \TYPO3\CMS\Core\Cache\Frontend\VariableFrontend::class,
        'backend' => \TYPO3\CMS\Core\Cache\Backend\TransientMemoryBackend::class,
        'options' => [],
        'groups' => []
    );
}
